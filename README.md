This is the result of a practical challenge given to me as part of a Frontend Web Developer application.

A live demo is available to view [here](https://tangent-kitties.netlify.com).

This project is a comprehensive demonstration of my technical abilities to:
- Setup Webpack.
- Write clean and efficient React components in ES2015 syntax while using Redux for global state management.
- Handling unconvential API responses.
- Write clean and efficient HTML and SCSS that meets the design and interactivity specifications, while maintaining awareness of potential browser compatibility issues.

## Installation

```bash
git clone https://bitbucket.org/beningreenjam/tangent-kitties.git
cd tangent-kitties
npm install
```

## Get Started

```bash
npm run start
```