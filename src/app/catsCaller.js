import 'whatwg-fetch';
import xmltojson from "xmltojson";

import {addCats} from "./actions";

class catsCaller {
    call() {
        fetch("//thecatapi.com/api/images/get?format=xml&size=med&results_per_page=9")
            .then(response => {
                response.blob().then(blob => this._untangleCats(blob, this._herdCats));
            });
    }

    /**
     * Decode ReadableStream to XML string
     *
     * @param blob
     * @param cb
     * @returns {*}
     */
    _untangleCats(blob, cb) {
        let reader = new FileReader();
        reader.addEventListener("loadend", function (result) {
            cb(result.target.result);
        });
        reader.readAsText(blob);
    }

    /**
     * Convert XML string to JSON
     *
     * @param xml
     * @private
     */
    _herdCats(xml) {
        let clowder = xmltojson.parseString(xml, {
            childrenAsArray: false,
            textKey: "text"
        });

        if (typeof clowder === "object" && clowder.hasOwnProperty("response") && typeof clowder.response === "object" && clowder.response.hasOwnProperty("data")) {
            const cats = clowder.response.data.images.image;

            if (Array.isArray(cats) && cats.length > 0) {
                addCats(cats);
            }
        }
    }
}

export default catsCaller;