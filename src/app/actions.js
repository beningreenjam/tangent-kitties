import {store} from "./store";

export const getCurrentUser = () => store.dispatch({type: "GET_USER"});
export const addCats = cats => store.dispatch({type: "ADD_CATS", data: cats});
export const toggleLike = (user,catId) => ({type: "TOGGLE_LIKE", data: {user,catId}});
export const updateErrors = (error) => ({type: "UPDATE_ERRORS", data: error});