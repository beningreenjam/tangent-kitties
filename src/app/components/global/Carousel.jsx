import React from "react";
import {Helmet} from "react-helmet";
import {connect} from "react-redux";
import Slick from "react-slick";

import Cat from "./Cat";
import CarouselLeftArrow from "./CarouselLeftArrow";
import CarouselRightArrow from "./CarouselRightArrow";

class Carousel extends React.Component {
    render() {
        let {cats,exclude,catName} = this.props,
            styleOverride = {
                fontSize: "97px",
                lineHeight: "normal",
                position: "absolute",
                top: "50%",
                width: "auto",
                height: "auto",
                color: "inherit"
            },
            settings = {
                accessibility: true,
                infinite: true,
                speed: 1000,
                easing: "easeOutBack",
                swipeToSlide: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                mobileFirst: true,
                prevArrow: <CarouselLeftArrow styleOverride={styleOverride}/>,
                nextArrow: <CarouselRightArrow styleOverride={styleOverride}/>,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            fade: true
                        }
                    },
                    {
                        breakpoint: 999,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            };

        return (
            <div className="o-container u-pt-x2-5 u-pb-x5">
                <Helmet>
                    <link rel="stylesheet"
                          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
                          integrity="sha256-jySGIHdxeqZZvJ9SHgPNjbsBP8roij7/WjgkoGTJICk=" crossorigin="anonymous"/>
                    <link rel="stylesheet"
                          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
                          integrity="sha256-WmhCJ8Hu9ZnPRdh14PkGpz4PskespJwN5wwaFOfvgY8=" crossorigin="anonymous"/>
                </Helmet>
                <h3 className="u-mb-x2-5 u-text-center u-text-left-when-sm">Other kitties related to ‘{catName}</h3>
                <div className="u-ph-x15">
                    <Slick {...settings}>{cats.map(({id, url, source_url}, index) => {
                        if(exclude && index == exclude) {
                            return;
                        }
                        return (
                            <div key={id.text}>
                                <Cat id={index} img={url.text} origin={source_url.text} mode="carousel"/>
                            </div>
                        )
                    })}</Slick>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({cats}) => ({cats});

export default connect(mapStateToProps)(Carousel);