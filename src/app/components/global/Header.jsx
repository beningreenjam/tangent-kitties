import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
    <div className="o-container u-pt">
        <header className="u-pt-x10 u-pt-x7-when-sm u-text-center u-position-relative">
            <Link to="/" className="u-decoration-none u-position-absolute u-position-absolute--top u-position-absolute--left u-position-absolute--right">
                <span className="c-icon c-icon--cat u-colour-light"></span>
                <strong className="u-display-inline-block u-pl u-pr-when-sm u-colour-darkest">Catopedia</strong>
                <small className="u-display-block u-display-inline-block-when-sm u-colour-light">Buy cats for cats</small>
            </Link>
            <div className="u-position-absolute u-position-absolute--top u-position-absolute--right">
                <span className="c-icon c-icon--hamburger u-colour-light"></span>
            </div>
        </header>
        <hr/>
    </div>
);

export default Header;