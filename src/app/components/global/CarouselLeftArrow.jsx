import React from "react";

class CarouselLeftArrow extends React.Component {
    render() {
        const {className, style, styleOverride, onClick} = this.props;

        return (
            <div className={className} style={{...style,...styleOverride,left: "-50px"}} onClick={onClick}>
                <span className="c-icon c-icon--left-arrow u-colour-primary"></span>
            </div>
        )
    }
}

export default CarouselLeftArrow;