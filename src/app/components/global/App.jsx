import React from "react";
import { Switch, Route } from "react-router-dom";

import Header from "./Header";
import Footer from "./Footer";
import HomePage from "../pages/HomePage";
import CatBioPage from "../pages/CatBioPage";
import NotFoundPage from "../pages/NotFoundPage";

import catsCaller from "../../catsCaller";
import { getCurrentUser } from "../../actions";

class App extends React.Component {
    componentWillMount() {
        getCurrentUser();
        const cats = new catsCaller();
        cats.call();
    }

    render() {
        return (
            <div className="u-pb-x15">
                <Header/>
                <main>
                    <Switch>
                        <Route exact path="/" component={HomePage}/>
                        <Route path="/:catId(\d)" component={CatBioPage} />
                        <Route path="*" component={NotFoundPage} />
                    </Switch>
                </main>
                <Footer/>
            </div>
        )
    }
}

export default App;