import React from "react";
import {Link} from "react-router-dom";

class Breadcrumbs extends React.Component {
    render() {
        let {breadcrumbs} = this.props;

        if(!breadcrumbs) {
            breadcrumbs = [];
        }

        breadcrumbs.unshift({path: "/", text: "cat-alogue"});
        breadcrumbs.unshift({path: "/", text: "Home"});

        breadcrumbs = breadcrumbs.map((breadcrumb, index) => {
            if (index === breadcrumbs.length-1) return <span key={index} className="u-colour-light">{breadcrumb.text}</span>;

            return (
                <span key={index} className="u-colour-light">
                    <Link to={breadcrumb.path} className="u-colour-light u-decoration-none">{breadcrumb.text}</Link>
                    <span>&nbsp;|&nbsp;</span>
                </span>
            )
        });

        return (
            <div className="u-display-hidden u-display-block-when-lg u-mt">{breadcrumbs}</div>
        )
    }
}

export default Breadcrumbs;