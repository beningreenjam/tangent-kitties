import React from "react";

class AspectRatio extends React.Component {
    render() {
        const style = {
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                backgroundImage: `url(${this.props.img})`
            },
            classList = this.props.classList;

        return (
            <div className="u-aspect-ratio--4-3">
                <div className={`content ${classList ? classList : ""}`} style={style}></div>
            </div>
        )
    }
}

export default AspectRatio;