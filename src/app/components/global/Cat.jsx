import React from "react";
import {Link} from "react-router-dom";

import LikeButton from "./LikeButton";
import AspectRatio from "./AspectRatio";

class Cat extends React.Component {
    render() {
        let {id, img, origin, mode} = this.props,
            containerClassList = (mode === "grid") ? "o-grid__col u-1-of-2-when-sm u-1-of-3-when-lg" : "u-bgcolour-lightest u-mh-x2-5",
            contentClassList = (mode === "grid") ? "u-mb-x5" : "u-mb-x10 u-box-shadow";

        return (
            <div className={containerClassList} title={`Courtesy of: ${origin}`}>
                <div className="u-p u-mb-x5 u-box-shadow">
                    <AspectRatio classList={contentClassList} img={img}/>
                    <div className="o-grid o-grid--middle u-mb-x2-5">
                        <div className="o-grid__col u-2-of-3">
                            <LikeButton key={id} catId={id}/>
                        </div>
                        <div className="o-grid__col u-1-of-3 u-text-right">
                            <Link to={`/${id}`} className="u-decoration-none u-colour-light">
                                <span className="c-icon c-icon--arrow-forward"></span>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Cat;