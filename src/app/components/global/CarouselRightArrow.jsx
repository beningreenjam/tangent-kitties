import React from "react";

class CarouselRightArrow extends React.Component {
    render() {
        let {className, style, styleOverride, onClick} = this.props;

        return (
            <div className={className} style={{...style,...styleOverride,right: "-50px"}} onClick={onClick}>
                <span className="c-icon c-icon--right-arrow u-colour-primary"></span>
            </div>
        )
    }
}

export default CarouselRightArrow;