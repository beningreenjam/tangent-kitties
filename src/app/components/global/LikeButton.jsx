import React from "react";
import {connect} from "react-redux";

import {toggleLike} from "../../actions";

class LikeButton extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    iconClassList(catLikes,user) {
        return `c-icon c-icon--favorite${catLikes.indexOf(user) >= 0 ? " u-colour-primary" : "-outline"}`;
    }

    handleClick() {
        let {user, catId, toggleLike} = this.props;
        toggleLike(user,catId);
    }

    render() {
        let {likes, user, catId} = this.props,
            catLikes = likes.hasOwnProperty(catId) ? likes[catId] : [];

        return (
            <div className="u-display-inline-block u-pl u-colour-light">
                <span className={this.iconClassList(catLikes,user)} onClick={this.handleClick}></span>
                <span className="u-display-inline-block u-ml">{catLikes.length} Likes</span>
            </div>
        )
    }
}

const mapStateToProps = ({likes, user}) => ({likes, user});

const mapDispatchToProps = dispatch => ({
    toggleLike: (user, catId) => dispatch(toggleLike(user, catId))
});

export default connect(mapStateToProps, mapDispatchToProps)(LikeButton);