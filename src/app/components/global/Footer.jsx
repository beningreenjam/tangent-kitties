import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
    <footer className="u-pv u-bgcolour-light u-position-absolute u-position-absolute--left u-position-absolute--bottom u-position-absolute--right">
        <div className="o-container o-container--md u-text-center u-text-left-when-sm">
            <Link to="/contact-us" className="u-colour-primary u-decoration-none u-display-block u-display-inline-block-when-sm">Contact Us</Link>
            <Link to="/careers" className="u-colour-primary u-decoration-none u-display-block u-display-inline-block-when-sm u-ml-x5-when-sm">Careers</Link>
        </div>
    </footer>
);

export default Footer;