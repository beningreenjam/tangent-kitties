import React from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import {updateErrors} from "../../actions";

class ErrorBoundary extends React.Component {
    componentDidCatch(error, info) {
        this.props.updateErrors(true);
        // Log error to error monitoring service
    }

    render() {
        let {error, children} = this.props;

        if (error) {
            return (
                <div className="u-text-center">
                    <h1>App Now Laying Broken On The Floor.</h1>
                    <p>Some cheeky cat thought it would be amusing to push the app off of the shelf and so it now lays
                        broken on the floor.</p>
                    <Link to="/" className="c-btn c-btn--primary">Reload App Home</Link>
                </div>
            );
        }
        return children;
    }
}

const mapStateToProps = ({error}) => ({error});

const mapDispatchToProps = dispatch => ({
    updateErrors: dispatch => dispatch(updateErrors(error))
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorBoundary);