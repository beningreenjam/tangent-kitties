import React from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import LikeButton from "./LikeButton";
import Breadcrumbs from "../global/Breadcrumbs";

class CatBio extends React.Component {
    render() {
        let {cats, catId, catName} = this.props,
            {url, source_url} = cats[catId];

        return (
            <div className="o-container">
                <Breadcrumbs breadcrumbs={[{path: catId, text: catName}]}/>
                <h1>{catName}</h1>
                <div className="o-container o-container--md u-colour-light">
                    <div className="o-grid o-grid--equal-height u-mh u-mh-x0-when-sm">
                        <div className="o-grid__col u-1-of-2-when-sm">
                            <div>
                                <img src={url.text} alt=""/>
                            </div>
                        </div>
                        <div className="o-grid__col u-1-of-2-when-sm u-position-relative">
                            <div className="u-mb-x5">
                                <h4 className="u-mb-x2-5">{catName} sub-heading</h4>
                                <p>"Look here, friend," said I, "if you have anything important to tell us, out with it; but if you are only trying to bamboozle us, you are mistaken in your game; that's all I have to say." "And it's said very well, and I like to hear a chap talk up that way; you are just the man for him—the likes of ye. Morning to ye, shipmates, morning! Oh! when ye get there, tell 'em I've concluded not to make one of 'em." "Ah, my dear fellow, you can't fool us that way—you can't fool us. It is the easiest</p>
                                <p>Courtesy of {source_url.text}</p>
                            </div>
                            <div className="u-position-absolute u-position-absolute--bottom u-position-absolute--left u-position-absolute--right">
                                <div className="o-grid">
                                    <div className="o-grid__col u-1-of-2">
                                        <div className="u-ml-x7">
                                            <LikeButton catId={catId}/>
                                        </div>
                                    </div>
                                    <div className="o-grid__col u-1-of-2 u-text-right">
                                        <Link to="/" className="u-colour-primary u-decoration-none">Back to cat-alogue</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({cats}) => ({cats});

export default connect(mapStateToProps)(CatBio);