import React from "react";
import {connect} from "react-redux";

import ErrorBoundary from "../global/ErrorBoundary";
import Breadcrumbs from "../global/Breadcrumbs";
import Cat from "../global/Cat";

const HomePage = ({cats}) => (
    <ErrorBoundary>
        <div className="o-container o-container--md">
            <Breadcrumbs/>
            <h1>Cat-alogue</h1>
            <div className="o-grid c-cat-grid">{
                cats.map(({id, url, source_url}, index) => {
                    return <Cat key={id.text} id={index} img={url.text} origin={source_url.text} mode="grid"/>
                })
            }</div>
            <div className="u-text-center">
                <div className="u-display-inline-block u-bgcolour-light u-border-radius u-border-radius-round u-p-x5">
                    <span className="c-icon c-icon--favorite u-colour-primary"></span>
                </div>
            </div>
        </div>
    </ErrorBoundary>
);

const mapStateToProps = ({cats}) => ({cats});

export default connect(mapStateToProps)(HomePage);