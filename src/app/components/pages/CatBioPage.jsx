import React from "react";

import CatBio from "../global/CatBio";
import Carousel from "../global/Carousel";
import ErrorBoundary from "../global/ErrorBoundary";

const CatBioPage = ({match}) => (
    <ErrorBoundary>
        <CatBio catId={match.params.catId} catName="Kitty moaw-moaw"/>
        <div className="u-bgcolour-light u-pv u-mt-x56 u-mb">
            <Carousel exclude={match.params.catId} catName="Kitty moaw-moaw"/>
        </div>
    </ErrorBoundary>
);

export default CatBioPage;