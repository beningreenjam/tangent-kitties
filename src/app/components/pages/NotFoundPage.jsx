import React from "react";

const NotFoundPage = () => (
    <div className="u-text-center">
        <h2>This Page Has Been Stolen By Kittens</h2>
    </div>
);

export default NotFoundPage;