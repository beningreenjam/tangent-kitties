import ADD_CATS from "./actions";

/**
 * Create dummy user - for toggling likes
 *
 * @param state
 * @param type
 * @returns {*}
 */
export const user = (state, {type}) => {
    switch (type) {
        case "GET_USER":
            return +new Date();
        default:
            return state || 0;
    }
};

/*
 * Add cats fetched from API
 */
export const cats = (state, {type, data}) => {
    switch (type) {
        case "ADD_CATS":
            return state.cats ? state.cats.concat(data) : data;
        default:
            return state || [];
    }
};

/*
 * Toggle user's like for cat
 */
export const likes = (state, {type, data}) => {
    switch (type) {
        case "TOGGLE_LIKE":
            let {user, catId} = data,
                likes = state;

            if (!likes.hasOwnProperty(catId)) {
                likes[catId] = [];
            }

            if (likes[catId].indexOf(user) < 0) {
                likes[catId].push(user);
            }
            else {
                likes[catId] = likes[catId].filter(userId => userId !== user);
            }
            return Object.assign({}, state, likes);
        default:
            return state || {};
    }
};

/*
 * Toggle display/clear friendly error notification
 */
export const error = (state, {type, data}) => {
    switch (type) {
        case "UPDATE_ERRORS":
            return data;
        default:
            return !!state;
    }
};